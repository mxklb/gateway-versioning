#!/bin/bash
# Walks trough all folders (=submodules) and pushes tags.
for dir in */ ; do
  dir="$(basename $dir)"
  cd $dir && git push --tags
  echo "Pushed tags to -> $dir"
  cd ..
done
