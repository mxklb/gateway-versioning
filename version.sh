#!/bin/bash
# This script prints a sematic version number in the form: "X.Y.Z"
# The version number is fetched from the latest git tag!
# Note: Git version tags must be in format "v1-0-0"
# Note: This script needs/depends on git.

pushd `dirname $0` > /dev/null
scriptPath=`pwd`
popd > /dev/null
cd "$scriptPath"

# Get version from latest git tag (uses 'v1-2-3' tags)
gitversion=$(git tag -l | tail -1)
IFS='-' read -a versions <<< "$gitversion"

# Set version if untagged ..
if [ -z "${versions[0]}" ]; then
  version="0.0.1"
else
  major=${versions[0]}
  if [[ $major == *"v"* ]]; then
    major="${major#?}"
  fi
  version=$(echo "$major.${versions[1]}.${versions[2]}")
fi

echo "$version"
