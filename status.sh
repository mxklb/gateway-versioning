#!/bin/bash
# Walks trough all folders and gets tags.
checkversion() {
  dir="$(basename $1)"
  currenttag=$(git describe --tags)
  git fetch --tags --prune
  latesttag=$(git tag -l | tail -1)
  echo "Fetched tags from -> $dir -- current $currenttag -- latest $latesttag"
}

# parent folder
dir=$(dirname "$(readlink -f "$0")")
checkversion $dir

# submodules
for dir in */ ; do
  cd $dir
  checkversion $dir
  cd ..
done
