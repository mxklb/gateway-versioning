#!/bin/bash
# Updates all submodules to its latest tag
for dir in */ ; do
  cd $dir
  dir="$(basename $dir)"
  git fetch --tags --prune
  latesttag=$(git tag -l | tail -1)
  git pull
  git checkout $latesttag
  echo "Updated submodule -> $dir -- $latesttag"
  cd ..
done
