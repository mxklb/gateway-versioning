# Gateway Versioning
This repository is used to version a system which consists of multiple git repositories.

It manages the versioning of multiple
[git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
It is used to track versions of submodules which are compatible to each other.
Think about some sort of parent repository. Each submodule shall be developed
independently and shall use its own CI/CD pipeline. This repository is used to
maintain a parent project which depends on compatible build artifacts of its
submodules.

[Git tags](https://git-scm.com/book/en/v2/Git-Basics-Tagging) are used for
versioning. Each repository shall use a [semantic version number](https://semver.org/)
tag

**vMAJOR-MINOR-PATCH**

Note: We use **-** as a version separator. MAJOR, MINOR, PATCH
are integer numbers.

*Best practice*: Automate release artifact deployments when a `git tag` is
pushed to a submodule.

# Setup
Clone this repository with all its submodules

    git clone --recurse-submodules -j8 git@gitlab.com:mxklb/gateway-versioning.git

Note: To add or remove submodules check the [git submodules man pages](https://git-scm.com/docs/git-submodule).

*Best practice*: Regularly call the ***status.sh*** script to check for changes ;)

# Usage
This repository is used to version a parent project which consists of multiple
independent git repositories. Each child project is integrated as a git submodule.
Submodule versions (git tags) could be managed by the submodules themselves and/or
directly here, from this repository.

## Check for submodule updates
If a submodule manages versioning by itself via git tags ..

**1. Check if a new version of a submodule is available**

    ./status.sh

This calls `git fetch --tags` for all submodules and prints out the current vs
the latest tag.

Note: This also reports untagged local changes, refer to `git describe --tags`.

**2. Update a submodule (checkout explicit tag or commit)**

Go into the submodule-folder you like to update and checkout the wanted version

    cd submodule-folder
    git pull && git checkout tagname

Note: Do this for each submodule you like to update.

Optionally to auto update all submodules to its latest tag, run

    ./update.sh

**3. Commit and tag your changes (bump global version)**

    git commit -m "Updated submodules XYZ & ABC"
    git tag -a tagname -m "Parent Release"
    git push --tags

Note: *tagname* shall be a semantic version number as described above.

## Manage submodule versions
To manage versions (set git tags) of submodules from this repository ..

**1. Go into the submodule-folder, checkout the wanted commit and tag it**

    cd submodule-folder
    git pull && git checkout commit-sha-to-be-tagged
    git tag -a tagname -m "New release of submodule XYZ"

Note: Do this for each submodule you like to tag.

**2. Push the tagged new version to the submodule**

    git push --tags

Note: This promotes the new tag to the submodule repository!

Optionally to auto push all new tags made to multiple submodules, try

    ./pushtags.sh

**3. Commit and tag your changes here (bump global version)**

    same as 3. above ..
